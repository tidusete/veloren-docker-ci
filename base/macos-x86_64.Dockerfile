ARG FROM_TAG=please-specify-a-tag-also-hopefully-this-tag-wont-be-used
FROM registry.gitlab.com/veloren/veloren-docker-ci/base/osxcross-x86_64:${FROM_TAG} as osxcross-forcopy
# FROM registry.gitlab.com/veloren/veloren-docker-ci/base/shaderc-macos-x86_64:${FROM_TAG} as shaderc-macos-forcopy
FROM registry.gitlab.com/veloren/veloren-docker-ci/base/common:$FROM_TAG

# Install clang (needed to cross compile to macos)
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
    && time apt-get install -y --no-install-recommends --assume-yes \
        clang \
    # Cleanup extra cached files
    && rm -rf /var/lib/apt/lists/*;

# Copy osxcross
# NOTE: linking fails if this is copied to a different path than what it was built at
COPY --from=osxcross-forcopy /osxcross/target/ /osxcross/target/
# Copy shaderc
# NOTE: not acually useful atm as linking fails
# COPY --from=shaderc-macos-forcopy /shaderc/ /shaderc/

# Install apple toolchain
RUN echo \
        "[target.x86_64-apple-darwin]\nlinker = \"/osxcross/target/bin/x86_64-apple-darwin17-clang\"\nar = \"/osxcross/target/bin/x86_64-apple-darwin17-ar\"\n" \
        >> /root/.cargo/config \
    && . /root/.cargo/env \
    && time rustup target add x86_64-apple-darwin
