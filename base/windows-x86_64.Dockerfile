FROM registry.gitlab.com/veloren/veloren-docker-ci/base/common:$FROM_TAG

# Install neccesary packages for windows cross compile
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
    && time apt-get install -y --no-install-recommends --assume-yes \
        # ~300 mb
        # mingw-w64 needed to cross compile to windows
        gcc-mingw-w64-x86-64 \
        # NOTE: these could be duplicated in linux images (consider common image with this package?)
        # ~86 mb
        gcc \
        # ~25 mb
        libc-dev \
        # NOTE: these are needed for building shaderc
        # ~60 mb
        cmake \
        # ~10 mb
        python3 \
        # ~17 mb (NOTE: will not be needed after upgrading shaderc)
        python \
        # ~122 mb
        g++-mingw-w64-x86-64 \
        # ~0.4 mb
        make \
    # Cleanup extra cached files
    && rm -rf /var/lib/apt/lists/*;

# Install windows gnu toolchain
RUN echo \
        "[target.x86_64-pc-windows-gnu]\nlinker = \"/usr/bin/x86_64-w64-mingw32-gcc\"\n" \
        >> /root/.cargo/config \
    && . /root/.cargo/env \
    && time rustup target add x86_64-pc-windows-gnu
