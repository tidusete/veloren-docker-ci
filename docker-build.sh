#!/bin/bash

# Make sure to commit before running
git diff-index -p --quiet HEAD --exit-code || (echo "Make sure to commit any changes before running!!! (check 'git status')" & exit 1);
# Get short version of commit hash
commit=$(git rev-parse --short HEAD);
if [[ ${#commit} < 7 ]]; then
    echo "failed to get commit hash, got this: $commit";
    exit 1;
fi

registry="registry.gitlab.com/veloren/veloren-docker-ci";
base_name="$registry/base";
cache_name="$registry/cache";
fetch_name="$registry/fetch";

# Make sure we have access to docker, if not set correct rights or use root user
docker login $registry;

# STAGE: base-start (grouped by ci build stages for organization)
# Build and push base/common image
time docker build -t $base_name/common:$commit --no-cache ./base/common;
docker push $base_name/common:$commit;
# # Build and push base/osxcross image
# time docker build -t $base_name/osxcross:$commit --no-cache ./base/osxcross;
# docker push $base_name/osxcross:$commit;
# Build and push base/shaderc-linux image
time docker build -t $base_name/shaderc-linux:$commit --no-cache ./base/shaderc/linux;
docker push $base_name/shaderc-linux:$commit;
# # Build and push base/shaderc-macos image
# time docker build -t $base_name/shaderc-macos:$commit --no-cache ./base/shaderc/macos;
# docker push $base_name/shaderc-macos:$commit;

# STAGE: base-platform
# Build and push base/linux image
time docker build -t $base_name/linux:$commit --no-cache --build-arg FROM_TAG=$commit ./base/linux;
docker push $base_name/linux:$commit;
# Build and push base/linux-aarch64 image
time docker build -t $base_name/linux-aarch64:$commit --no-cache --build-arg FROM_TAG=$commit ./base/linux-aarch64;
docker push $base_name/linux-aarch64:$commit;
# Build and push base/windows image
time docker build -t $base_name/windows:$commit --no-cache --build-arg FROM_TAG=$commit ./base/windows;
docker push $base_name/windows:$commit;
# # Build and push base/macos image
# time docker build -t $base_name/macos:$commit --no-cache --build-arg FROM_TAG=$commit ./base/macos;
# docker push $base_name/macos:$commit;

# STAGE: cache
# Build and push cache/bench image
time docker build -t $cache_name/bench:$commit --no-cache --build-arg FROM_TAG=$commit ./cache/bench;
docker push $cache_name/bench:$commit;
# Build and push cache/quality image
time docker build -t $cache_name/quality:$commit --no-cache --build-arg FROM_TAG=$commit ./cache/quality;
docker push $cache_name/quality:$commit;
# Build and push cache/tarpaulin image
time docker build -t $cache_name/tarpaulin:$commit --no-cache --build-arg FROM_TAG=$commit ./cache/tarpaulin;
docker push $cache_name/tarpaulin:$commit;
# Build and push cache/release-linux image
time docker build -t $cache_name/release-linux:$commit --no-cache --build-arg FROM_TAG=$commit ./cache/release/linux;
docker push $cache_name/release-linux:$commit;
# Build and push cache/release-linux-aarch64 image
time docker build -t $cache_name/release-linux-aarch64:$commit --no-cache --build-arg FROM_TAG=$commit ./cache/release/linux-aarch64;
docker push $cache_name/release-linux-aarch64:$commit;
# Build and push cache/release-windows image
time docker build -t $cache_name/release-windows:$commit --no-cache --build-arg FROM_TAG=$commit ./cache/release/windows;
docker push $cache_name/release-windows:$commit;
# # Build and push cache/release-macos image
# time docker build -t $cache_name/release-macos:$commit --no-cache --build-arg FROM_TAG=$commit ./cache/release/macos;
# docker push $cache_name/release-macos:$commit;

